<!DOCTYPE html>

<!-- DATABASE AND SERVER SETUP -->
<?php
	// These two lines will connect the code to the MEMBER_HISTORY mysql
	// database on the localhost machine.
	$SQL_database = "MEMBER_HISTORY";	// Set the name of the db to be accessed
	include "Connect.php";				// Include the file to make magic happen
?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="Global-Header.css" />
	<link rel="stylesheet" type="text/css" href="Theme.css" />
</head>



<body>
	<!-- INCLUDE THE PAGE HEADER -->
	<?php include "Global-Header.php"; ?>


<div id="contents">
	<?php
		// Get a table of all Team members
		$SQLcommand = "SELECT ID, FirstName, LastName, ClassOf FROM Members";
		$result = $SQL_conn->query($SQLcommand);

		// Print a list of all Members
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				echo "Name: " . "<a href=\"MemberInfo.php?user=" . $row['ID'] . "\">" . $row["FirstName"]. " " . $row["LastName"] . "</a><br/>";
			}
		}
	?>
</div>
</body>
</html>