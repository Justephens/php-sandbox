<!DOCTYPE>

<!-- DATABASE AND SERVER SETUP -->
<?php
	// These two lines will connect the code to the MEMBER_HISTORY mysql
	// database on the localhost machine.
	$SQL_database = "MEMBER_HISTORY";	// Set the name of the db to be accessed
	include "Connect.php";				// Include the file to make magic happen
?>

<!-- FUNCTIONS FOR PRINTING SEARCH RESULTS -->
<?php
	function printResults($name, $id)
	{
		// Prints the name, which links to a detailed page
		echo "<div class=\"search_result\">";
		echo "<a href=\"MemberInfo.php?user=" . $id . "\">" . $name . "</a>";
		echo "</div>";
	}
?>

<html>
<head>
	<!-- INCLUDE STYLESHEETS -->
	<link rel="stylesheet" type="text/css" href="Global-Header.css" />
	<link rel="stylesheet" type="text/css" href="Theme.css" />
</head>



<body>
	<!-- INCLUDE THE PAGE HEADER -->
	<?php include "Global-Header.php"; ?>



<div id="contents">
	<!-- CHECK FOR AND HANDLE INVALID QUERY -->
	<?php if (!isset($_GET["q"]) || empty($_GET["q"])): ?>

		<!-- IF THERE WAS NO SEARCH SHOW MESSAGE -->
		<h1> No Search Terms </h1>
		<p> There are no terms to search for. Please enter your search in the top right. </p>
		
		<!-- AUTO-FOCUS ON SEARCH BAR -->
		<script type="text/javascript">
			document.getElementById("headersearch").focus();
		</script>
	<?php ?>


	<!-- HANDLES ANY VALID QUERY -->
	<?php else:

		// Gets the raw search contents, and then splits it into
		// an array of keywords to be checked for
		$searchraw = $_GET["q"];
		$keywords = preg_split("/[\s,]+/", $searchraw);


		// Gets data about all members in the database. PHP will
		// narrow down the searches using string comparisons that
		// are beyond the ability of SQL
		$sql = "SELECT * FROM Members";
		$result = $SQL_conn->query($sql);


		// Makes an array that will use the row's ID as the key
		// and hold the "score" (number of matches) as the value
		$matches = array();


		// Loop through every table entry
		while ($row = $result->fetch_assoc()) {

			// A row's "score" is the number of matches between
			// the data in that row and the keywords in the query
			$score = 0;
			

			// Check each keyword against each field, counting the
			// number of matches, both complete and partial
			foreach ($keywords as $word)
				foreach ($row as $field)
					if (preg_match("/" . $word . "/i", $field) == 1)
						$score++;


			// If the score is greater than 0 (meaning matches
			// exist), add this row to the list of matches
			if ($score > 0)
				$matches[$row["ID"]] = $score;
		}


		// Sort matches into the proper order, with the highest
		// scoring rows first
		arsort($matches);


		// Prints a title for the search
		echo "<h1> Showing Results for \"" . $searchraw . "\"</h1>";


		// Print each match to the document. Each match is printed
		// with a link to a more detailed overview of that user
		foreach($matches as $x => $x_value) {
			mysqli_data_seek($result, $x-1);
			$row = $result->fetch_array();

			printResults($row[1] . " " . $row[2], $x);
		}


		// Finally, free the results recieved from the SQL query
		mysql_free_result($result);
	endif; ?>
</div>
</body>
</html>
