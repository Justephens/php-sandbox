<?php
// If any of the SQL variables are not set (being NULL or undefined)
// then set them to their defaults. This means if they already hold
// a value they will retain it, and not be set to the default. This
// allows for these variables to be set to a custom value (they would
// be set before including this file)
if (!isset($SQL_servername))	{ $SQL_servername	= "localhost"; }
if (!isset($SQL_username))		{ $SQL_username		= "root"; }
if (!isset($SQL_password))		{ $SQL_password		= "nutnut4682"; }

// The connection should always begin as NULL, but make sure to unset
// an existing connection object if there is one
if (isset($SQL_conn))	{ unset($conn); }
$SQL_conn = NULL;





// Creates a connection object, representing the mysql connection with
// the localhost
$SQL_conn = new mysqli($SQL_servername, $SQL_username, $SQL_password, $SQL_database);

// Test the connection for errors
if ($SQL_conn->connect_error) {
	// If the connection failed print why
	echo("Connect Error: " . $SQL_conn->connect_error);
	exit(0);
}
?>