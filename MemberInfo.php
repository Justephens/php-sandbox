<!DOCTYPE html>

<!-- DATABASE AND SERVER SETUP -->
<?php
	// These two lines will connect the code to the MEMBER_HISTORY mysql
	// database on the localhost machine.
	$SQL_database = "MEMBER_HISTORY";	// Set the name of the db to be accessed
	include "Connect.php";				// Include the file to make magic happen
?>

<html>
<head>
	<link rel="stylesheet" type="text/css" href="Global-Header.css" />
	<link rel="stylesheet" type="text/css" href="Theme.css" />
</head>



<body>
	<!-- INCLUDE THE PAGE HEADER -->
	<?php include "Global-Header.php" ?>



<div id="contents">
	<!-- PRINTS NAME OF USER-->
	<?php
		// Gets data on the user. Specifically, the user's name
		// and what year of school they are in
		$sql = "SELECT FirstName, LastName, ClassOf FROM Members WHERE ID = " . $_GET["user"];
		$result = $SQL_conn->query($sql);


		// If no user was found, give a basic error message that
		// describes what could have broke
		if ($result->num_rows != 1):
			// Print a title alerting that no user was found
			echo "<h1> No User Found </h1>";
			echo "<p> It is possible this user has been removed from the database, or never existed in the database to begin with. Verify the URL tag \"user\" has a valid ID number. </p>";


		// Otherwise, if a user did exist, print some facts about
		// that particular user
		else:
			$user = $result->fetch_assoc();

			// Print the user's name as the title
			echo "<h1> " . $user["FirstName"] . " " . $user["LastName"] . "</h1>";

			// Print the user's class year
			echo "Class of: " . $user["ClassOf"] . "<br/>";
		endif;
	?>

</div>
</body>
</html>